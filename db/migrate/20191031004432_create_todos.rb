class CreateTodos < ActiveRecord::Migration[6.0]
  def change
    create_table :todos do |t|
      t.string :item
      t.json :options
      t.timestamps
    end
  end
end
