import React from "react"
let nameIndex = 0
let data = {
  id: 0,
  options: {}
}

class CustomForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      token:'',
      todos: [],
      options: {},
      data: {}//
    }
    this.saveData = this.saveData.bind(this)
    this.handleInput = this.handleInput.bind(this)
    this.addOption = this.addOption.bind(this)
  }

  async saveData (id, options){
    data.id = id
    data.options = options

    Object.values(this.state.options).map((d) => {
      // assing data to options
      Object.assign(data.options, {[`${d}`]:""})
    })

    if (this.state.data) {
      Object.keys(this.state.data).map((s) => {
        Object.keys(data.options).map((d) => {
          if (s === d) {
            data.options[`${d}`] = this.state.data[`${s}`]
          }
        })
      })
      console.log(data, this.state.data)
    }

    // console.log(this.state)
    let url = '/todos/reactjson'
      // {
      //   'Content-Type': 'application/json',
      //   'X-Requested-With': 'XMLHttpRequest',
      //   'X-CSRF-Token': document.getElementsByName('csrf-token')[0].content
      //   // 'Content-Type': 'application/x-www-form-urlencoded',
      // }
    // csrf-token => rails token
     // data: {id: 10, options: {facebook: 'juan'}
    try {
      let response = await fetch(url, {
        method: 'post',
        credentials: "same-origin",
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
          'X-CSRF-Token': document.getElementsByName('csrf-token')[0].content
        },
        body: JSON.stringify(data)
      })

      // let data = await response.json()
      // console.log()
      // await this.setState({todos: await response.json()})
      await this.setState({options: {}}) //reset options for new option
      // window.reload()
    } catch (e) {
        console.log(e)
    }

  }

  async handleInput (e) {
    let {value} = e.target

    await this.setState(prevState => ({
      options: {
        ...prevState.options,
        [`name${nameIndex}`]: value
      }
    }))
  }

  async optionInput (d) {
    let {value, key} = d
    await this.setState(prevState => ({
      data: {
        ...prevState.data,
        [key]: value
      }
    }))
  }

  addOption (item) {
    let {id, options} = item
    nameIndex += 1
    this.saveData(id, options)
    // console.log(this.state)
  }

  async componentDidMount(){
  await this.setState({todos: this.props.todos})
  // debugger
  }

  render () {
    return (
      <div>
        <label className="label"><b>Custom Todo Form</b></label>
        {this.state.todos.map((t, i)=>{
          return(
            <div>
              <label key={i} className="label"><b>{t.item}</b></label>
              <input className="input" type="text"  onChange={this.handleInput} placeholder="option" />
              <button  className="button is-danger is-fullwidth" onClick={() => this.addOption(t)}>option</button>
            </div>
          )
        })}
        <hr/>
        <hr/>
        <hr/>
        <hr/>
        {this.state.todos.map((t, i) => {
          return(
            <div>
            <hr/>
              <label key={i} className="label"><b>{t.item}</b></label>
              {Object.keys(t.options).map((s) => {
                return (
                  <div>
                    <label className="label"><b>{s}</b></label>

                    <input className="input" type="text"  onChange={
                      (e) => this.optionInput({value: e.target.value, key: s})}
                      placeholder="value" />

                    <button className="button is-danger is-fullwidth" onClick={() => this.addOption(t)}>save</button>
                  </div>
                )
              })
              }
            </div>
          )
        })}
      </div>
    )
  }
}

// <label className="label"><b>option</b></label>
// <input className="input" type="text" onChange={this.handleInput} placeholder="option" />
// <button className="button is-danger is-fullwidth" onClick={this.addOption}>Add Option</button>
// {Object.values(this.state.options).map((d, i)=> {
//   return(<label key={i} className="label"><b>{d}</b></label>)
// })}

export default CustomForm
