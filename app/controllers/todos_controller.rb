class TodosController < ApplicationController
    before_action :set_todo, only: [:edit, :update, :destroy]
    def index

    end
    def new
        @todo = Todo.new
    end

    def edit
        @todo = Todo.find(params[:id])
    end


    def create
        @todo = Todo.new(todo_params)
        if @todo.save
            flash[:notice] = "¡Tarea agregada!"
            redirect_to root_path

        else
            flash[:warning] = "Trata de nuevo, hubo un error."
            redirect_to root_path
        end
    end

    def update
        @todo = Todo.find(params[:id])

        if @todo.update(todo_params)
            redirect_to root_path
        else
            redirect_to root_path
        end
    end

    def show

    end
    def reactjson
      @todo = Todo.find(params[:id])

      if @todo.update(todo_params)
          redirect_to todos_path
      else
          redirect_to root_path
      end
    end

    def destroy
        @todo = Todo.find(params[:id])
        @todo.destroy
        redirect_to root_path
    end
    def set_todo
      @todo = Todo.find(params[:id])
    end


    private
    def todo_params
        params.require(:todo).permit(:item, :options => {})
    end


end
